/*
KTHXBYE License Version 2.0!

 * Do NOT slap your corporation's license header anywhere, it's the law :D
 * You can totally do this:
   - fork it, spoon it, knife it
   - redistribute it, in source, binary, pizza or lasagne form
   - modify it, convert it, remix it, blend it
 * ALWAYS retain the CREDITS file, it credits the sources for all art, namely
   - The AWESOME Kenney aka Asset Jesus (http://www.kenney.nl)
   - "Bacterial Love" by RoleMusic (http://freemusicarchive.org/music/Rolemusic/Pop_Singles_Compilation_2014/01_rolemusic_-_bacterial_love)
 * ALWAYS retain this LICENSE file and any related material such as license headers.
 * IF USED FOR COMMERCIAL PURPOSES (including training material, talks, etc.) YOU MUST:
   - Take a photo of you wearing a pink hat, standing on one leg, holding a turtle
     - Turtle may be substituted by chicken, polar bear, or great white shark
   - Post the photo to Twitter along with the message "Am I pretty @badlogicgames?"

 If you violate this license, Karma will be a bitch (and i'll be petty on Twitter)!

Kthxbye, <3 Mario (Zechner, Copyright 2014-2234)
*/

package com.badlogicgames.plane;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class PlaneGame implements Screen {
	final GameScene game;



	ShapeRenderer shapeRenderer;
	SpriteBatch batch;
	OrthographicCamera camera;
	OrthographicCamera uiCamera;
	Texture background;
	Texture turbohud;
	Texture turbocharge;

	TextureRegion ground;
	float groundOffsetX = 0;
	TextureRegion ceiling;
	TextureRegion rock;
	TextureRegion rockDown;
	TextureRegion ready;
	TextureRegion gameOver;
	BitmapFont font;
	Plane plane;


	Array<Rock> rocks = new Array<Rock>();
	
	GameState gameState = GameState.Start;
	int score = 0;

	Rectangle rect = new Rectangle();
	
	Music music;
	Sound explode;
	
	public PlaneGame(final GameScene _game) {
		this.game = _game;
		shapeRenderer = new ShapeRenderer();
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		uiCamera = new OrthographicCamera();
		uiCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		uiCamera.update();
		
		font = new BitmapFont(Gdx.files.internal("arial.fnt"));
		
		background = new Texture("background.png");	
		ground = new TextureRegion(new Texture("ground.png"));
		ceiling = new TextureRegion(ground);
		ceiling.flip(true, true);
		
		rock = new TextureRegion(new Texture("rock.png"));
		rockDown = new TextureRegion(rock);
		rockDown.flip(false, true);
		

		ready = new TextureRegion(new Texture("ready.png"));
		gameOver = new TextureRegion(new Texture("gameover.png"));
		turbohud = new Texture("turbohud.png");
		turbocharge = new Texture("turbocharger.png");
		plane = new Plane();
		
		music = Gdx.audio.newMusic(Gdx.files.internal("music.mp3"));
		music.setLooping(true);
		music.play();
		
		explode = Gdx.audio.newSound(Gdx.files.internal("explode.wav"));
		
		resetWorld();
	}
	
	private void resetWorld() {
		score = 0;
		groundOffsetX = 0;
		plane.Reset();
		camera.position.x = 400;
		
		rocks.clear();
		for(int i = 0; i < 5; i++) {
			boolean isDown = MathUtils.randomBoolean();
			rocks.add(new Rock(700 + i * 200, isDown?480-rock.getRegionHeight(): 0, isDown? rockDown: rock));
		}
	}
	
	private void updateWorld() {
		float deltaTime = Gdx.graphics.getDeltaTime();

		
		if(Gdx.input.justTouched()) {
			if(gameState == GameState.Start) {
				gameState = GameState.Running;
			}
			if(gameState == GameState.Running) {
				plane.Jump();
			}
			if(gameState == GameState.GameOver) {
				gameState = GameState.Start;
				resetWorld();
			}
		}

		if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
			if(gameState == GameState.Running) {
				plane.BecomeInvulnerable();
			}
		}

			
		if(gameState != GameState.Start) plane.AddGravity();

		plane.Update(deltaTime);


		camera.position.x = plane.GetX() + 350;
		if(camera.position.x - groundOffsetX > ground.getRegionWidth() + 400) {
			groundOffsetX += ground.getRegionWidth();
		}
				

		for(Rock r: rocks) {
			if(camera.position.x - r.position.x > 400 + r.image.getRegionWidth()) {
				boolean isDown = MathUtils.randomBoolean();
				r.position.x += 5 * 200;
				r.position.y = isDown?480-rock.getRegionHeight(): 0;
				r.image = isDown? rockDown: rock;
				r.counted = false;
			}
			rect.set(r.position.x + (r.image.getRegionWidth() - 30) / 2 + 20, r.position.y, 20, r.image.getRegionHeight() - 10);
			if(plane.CheckColision(rect)) {
				if(gameState != GameState.GameOver) explode.play();
				gameState = GameState.GameOver;
				plane.Stop();
			}
			if(r.position.x < plane.GetX()  && !r.counted) {
				score++;
				r.counted = true;
			}
		}
		
		if(plane.GetY() < ground.getRegionHeight() - 20 || plane.GetY() + plane.GetHeight() > 480 - ground.getRegionHeight() + 20) {
			if(gameState != GameState.GameOver) explode.play();
			gameState = GameState.GameOver;
			plane.Stop();
		}		
	}
	
	private void drawWorld() {
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(background, camera.position.x - background.getWidth() / 2, 0);
		for(Rock rock: rocks) {
			batch.draw(rock.image, rock.position.x, rock.position.y);
		}
		batch.draw(ground, groundOffsetX, 0);
		batch.draw(ground, groundOffsetX + ground.getRegionWidth(), 0);
		batch.draw(ceiling, groundOffsetX, 480 - ceiling.getRegionHeight());
		batch.draw(ceiling, groundOffsetX + ceiling.getRegionWidth(), 480 - ceiling.getRegionHeight());
		plane.Draw(batch);
		batch.end();

		batch.setProjectionMatrix(uiCamera.combined);
		batch.begin();		
		if(gameState == GameState.Start) {
			batch.draw(ready, Gdx.graphics.getWidth() / 2 - ready.getRegionWidth() / 2, Gdx.graphics.getHeight() / 2 - ready.getRegionHeight() / 2);
		}
		if(gameState == GameState.GameOver) {
			batch.draw(gameOver, Gdx.graphics.getWidth() / 2 - gameOver.getRegionWidth() / 2, Gdx.graphics.getHeight() / 2 - gameOver.getRegionHeight() / 2);
		}
		if(gameState == GameState.GameOver || gameState == GameState.Running) {
			font.draw(batch, "" + score, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 60);
			//font.draw(batch, "" + plane.invulnerableCount, 50,  50);
			batch.draw(turbohud, 25, 10);
			for(int i = 0; i< plane.invulnerableCount; i++){
				batch.draw(turbocharge, 52 + (turbocharge.getWidth() + 4)*i , 27);
			}
		}
		batch.end();
	}



	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		updateWorld();
		drawWorld();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	static class Rock {
		Vector2 position = new Vector2();
		TextureRegion image;
		boolean counted;
		
		public Rock(float x, float y, TextureRegion image) {
			this.position.x = x;
			this.position.y = y;
			this.image = image;
		}
	}
	
	static enum GameState {
		Start, Running, GameOver
	}
}
