package com.badlogicgames.plane;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by damviod on 22/04/16.
 */
public class Plane {
    private static final float PLANE_START_Y = 240;
    private static final float PLANE_START_X = 50;
    private static final float PLANE_JUMP_IMPULSE = 350;
    private static final float GRAVITY = -20;
    private static final float PLANE_VELOCITY_X = 200;
    private static final float PLANE_VELOCITY_INVULNERABLE_X = 300;
    private static final float INVULNERABILITY_TIME = 3f;
    private static final int INVULNERABILY_MAX_COUNT = 3;
    private static final float TIME_TO_RELOAD_FUEL = 5f;

    Animation anim;
    Vector2 planePosition = new Vector2();
    Vector2 planeVelocity = new Vector2();
    Vector2 gravity = new Vector2();
    float planeStateTime = 0;
    Rectangle colision = new Rectangle();

    boolean isInvulnerable;
    int invulnerableCount ;
    float invulnerableTimeLeft;
    float reloadFuelTimeLeft;

    public Plane(){
        Texture frame1 = new Texture("plane1.png");
        frame1.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Texture frame2 = new Texture("plane2.png");
        Texture frame3 = new Texture("plane3.png");

        anim = new Animation(0.05f, new TextureRegion(frame1), new TextureRegion(frame2), new TextureRegion(frame3), new TextureRegion(frame2));
        anim.setPlayMode(Animation.PlayMode.LOOP);
        isInvulnerable = false;
        invulnerableTimeLeft = INVULNERABILITY_TIME;
        reloadFuelTimeLeft = TIME_TO_RELOAD_FUEL;
        invulnerableCount =INVULNERABILY_MAX_COUNT;
    }

    public void Update(float deltaTime){
        planeStateTime += deltaTime;

        if(isInvulnerable){
            planeVelocity.set(PLANE_VELOCITY_INVULNERABLE_X,0);
            invulnerableTimeLeft-=deltaTime;
            if(invulnerableTimeLeft < 0){
                isInvulnerable = false;
            }
        }
        else{
            if(invulnerableCount < INVULNERABILY_MAX_COUNT){
                reloadFuelTimeLeft-=deltaTime;
                if(reloadFuelTimeLeft < 0){
                    invulnerableCount++;
                    reloadFuelTimeLeft = TIME_TO_RELOAD_FUEL;
                }
            }
        }
        planePosition.mulAdd(planeVelocity, deltaTime);
        colision.set(GetX() + 20, GetY(), GetWidth() - 20, GetHeight());



    }

    public void Draw(SpriteBatch batch){
        batch.draw(anim.getKeyFrame(planeStateTime), planePosition.x, planePosition.y);

    }

    public void Reset(){
        planePosition.set(PLANE_START_X, PLANE_START_Y);
        planeVelocity.set(0, 0);
        gravity.set(0, GRAVITY);
        invulnerableCount =3;
        isInvulnerable = false;

        invulnerableTimeLeft = INVULNERABILITY_TIME;
        reloadFuelTimeLeft = TIME_TO_RELOAD_FUEL;
        invulnerableCount =INVULNERABILY_MAX_COUNT;
    }

    public void AddGravity(){


        planeVelocity.add(gravity);

    }


    public void Jump(){
        if(isInvulnerable){
            isInvulnerable = false;
        }
        planeVelocity.set(PLANE_VELOCITY_X, PLANE_JUMP_IMPULSE);
    }

    public void BecomeInvulnerable(){
        if(!isInvulnerable && invulnerableCount>0){
            isInvulnerable = true;
            invulnerableCount--;
            invulnerableTimeLeft = INVULNERABILITY_TIME;
        }
    }

    public void Stop(){
        planeVelocity.x = 0;
    }
    public float GetX(){
        return planePosition.x;
    }

    public float GetY(){
        return planePosition.y;
    }

    public float GetWidth(){
        return anim.getKeyFrames()[0].getRegionWidth();
    }

    public float GetHeight(){
        return anim.getKeyFrames()[0].getRegionHeight();
    }

    public boolean CheckColision(Rectangle other){
        if(isInvulnerable)
            return false;
        return colision.overlaps(other);
    }






}
