package com.badlogicgames.plane;

/**
 * Created by damviod on 22/04/16.
 */
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

public class MainMenuScreen implements Screen {
    final GameScene game;
    Texture button;
    OrthographicCamera camera;
    Music music;
    float xButton,yButton;
    public MainMenuScreen(final GameScene _game) {
        game = _game;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        button = new Texture(Gdx.files.internal("button_play.png"));
        xButton = Gdx.graphics.getWidth() /2 -button.getWidth()/2;
        yButton = Gdx.graphics.getHeight()/2 -button.getHeight()/2;
        music = Gdx.audio.newMusic(Gdx.files.internal("mainmenu.mp3"));
        music.setLooping(true);
        music.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.draw(game.batch, "Welcome to Plane!!! ",  Gdx.graphics.getWidth() /2 -65 ,  Gdx.graphics.getHeight()/2 + 50);
        game.batch.draw(button,  xButton ,  yButton);
        game.batch.end();

        if (Gdx.input.isTouched()) {
            if(Gdx.input.getX() > xButton && Gdx.input.getX() < xButton + button.getWidth()  &&
                    Gdx.input.getY() > yButton && Gdx.input.getY() < yButton + button.getHeight() ) {
                game.setScreen(new PlaneGame(game));
                dispose();
            }
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        music.dispose();
    }
}
